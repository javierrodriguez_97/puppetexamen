class wwwroot {

  file { '/var/www/':
    ensure => directory,
  }
  
  file { '/var/www/info.php':
    ensure => file,
    replace => true,
    require => File ["/var/www/"],
    source => "puppet://$::server/modules/wwwroot/info.php",
  }

  file { '/var/www/index.php':
    ensure => file,
    replace => true,
    require => File ["/var/www/"],
    content => template("wwwroot/index.php.erb")
  }

}

